<?php
if (!defined('_PS_VERSION_')) {
  exit;
}

class DatabaseOptimize extends Module
{
  protected $tables = [];

  public function __construct()
  {
    $this->name = 'databaseoptimize';
    $this->tab = 'front_office_features';
    $this->version = '1.0.0';
    $this->author = 'Mateusz Prasał';
    $this->need_instance = 0;
    $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    $this->bootstrap = true;
    $this->tables = [
      [
        'id' => 'cart',
        'name' => $this->l('Koszyki')
      ],
      [
        'id' => 'connections',
        'name' => $this->l('Połączenia')
      ],
      [
        'id' => 'connections_page',
        'name' => $this->l('Połączniea (page)')
      ],
      [
        'id' => 'connections_source',
        'name' => $this->l('Połączenia (source)')
      ],
      [
        'id' => 'guest',
        'name' => $this->l('Goście')
      ]
    ];
    parent::__construct();

    $this->displayName = $this->l('Database Optimize');
    $this->description = $this->l('Optimize your Prestashop DB');

    $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');


  }

  public function install()
  {
    if (!parent::install()) {
      return false;
    }

    return true;
  }

  public function uninstall()
  {
    if (!parent::uninstall()) {
      return false;
    }

    return true;
  }

  public function getContent()
  {

    $forms = $this->_displayForm();
    $cronUrls = [];
    foreach ($this->tables as $t) {
      $url = (Configuration::get('PS_SSL_ENABLED') ? $this->context->shop->getBaseURL(true) : $this->context->shop->getBaseURL()).'modules/databaseoptimize/databaseoptimize-cron.php?t='.$t['id'].'&token='.substr(Tools::encrypt('databaseoptimize/cron'), 0, 10);
      $cronUrls[] = $this->displayConfirmation("wget -O - '" . $url . "'");
    }

    $this->context->smarty->assign([
      'forms' => $forms,
      'tables' => $this->tables,
      'cronUrls' => $cronUrls
    ]);

    return $this->_postProcess() . $this->display(__FILE__,'config.tpl');
  }

  public function _displayForm()
  {
    $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

    $helper = new HelperForm();

    // Module, token and currentIndex
    $helper->module = $this;
    $helper->name_controller = $this->name;
    $helper->token = Tools::getAdminTokenLite('AdminModules');
    $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

    // Language
    $helper->default_form_language = $default_lang;
    $helper->allow_employee_form_lang = $default_lang;

    // Title and toolbar
    $helper->title = $this->displayName;
    $helper->show_toolbar = true;        // false -> remove toolbar
    $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
    $helper->submit_action = 'submit'.$this->name;
    $helper->toolbar_btn = array(
        'save' =>
        array(
            'desc' => $this->l('Save'),
            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
            '&token='.Tools::getAdminTokenLite('AdminModules'),
        ),
        'back' => array(
            'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Back to list')
        )
    );

    // Load current value
    $helper->fields_value[$this->name . '_table'] = Configuration::get($this->name . '_table');
    $helper->fields_value[$this->name . '_tableCron'] = Configuration::get($this->name . '_tableCron');

    return $helper->generateForm($this->_renderForm());

  }

  public function _postProcess()
  {

    if (Tools::isSubmit('submit' . $this->name)) {

      $errors = [];
      $output = '';
      if ($this->optimize(Tools::getValue($this->name . '_table'))) {
        $output = $this->displayConfirmation($this->l('Optymalizacja powiodła się'));
      } else {
        $errors[] = $this->displayError($this->l('Wystąpił błąd'));
      }

      if (!empty($errors)) {
        foreach ($errors as $error) {
          $output .= $error;
        }
      }

      return $output;
    }


  }

  public function _renderForm()
  {
    $fields_form[0]['form'] = array(
        'legend' => array(
            'title' => $this->l('Ustawienia'),
        ),
        'input' => array(
          array(
            'type' => 'select',                              // This is a <select> tag.
            'label' => $this->l('Tabela'),         // The <label> for this <select> tag.

            'name' => $this->name . '_table',                     // The content of the 'id' attribute of the <select> tag.
            'required' => true,                              // If set to true, this option must be set.
            'options' => array(
              'query' => $this->tables,                           // $options contains the data itself.
              'id' => 'id',                           // The value of the 'id' key must be the same as the key for 'value' attribute of the <option> tag in each $options sub-array.
              'name' => 'name'                               // The value of the 'name' key must be the same as the key for the text content of the <option> tag in each $options sub-array.
              )
            ),
        ),
        'submit' => array(
            'title' => $this->l('Optymalizuj'),
            'class' => 'btn btn-default pull-right'
        ),

    );

    return $fields_form;
  }

  public function optimize($table)
  {

    Configuration::updateValue($this->name . '_optimizeDate_' . $table, date('Y-m-d H:i:s'));
    Db::getInstance()->execute('DELETE FROM `' . _DB_PREFIX_ . $table . '`');

    return true;
  }

  public function checkLastOptimize($table)
  {

    return Configuration::get($this->name . '_optimizeDate_' . $table);
  }

  public function getCount($table)
  {
    $query = Db::getInstance()->executeS('SELECT COUNT(*) AS counter FROM `' . _DB_PREFIX_ . $table . '`');
    return $query[0]['counter'];
  }

}
