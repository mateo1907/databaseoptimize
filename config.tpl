
{$forms}

<div class="panel">
  <div class="panel-heading">
    {l s='Statystyki optymalizacji - Ostatnie uruchomienia'}
  </div>
  <div class="form-wrapper">

    {foreach from=$tables item=table }
      <p>{$table.name} <strong>({Module::getInstanceByName('databaseoptimize')->getCount($table.id)})</strong> : <strong>{if Module::getInstanceByName('databaseoptimize')->checkLastOptimize($table.id) == false} {l s='Nie uruchomiono optymalizacji'} {else} {Module::getInstanceByName('databaseoptimize')->checkLastOptimize($table.id)}  {/if}</strong> </p>
    {/foreach}
  </div>
</div>

<div class="panel">
  <div class="panel-heading">
    {l s='URL Cron'}
  </div>
  <div class="form-wrapper">
    {foreach from=$cronUrls item=url}
      {$url}
    {/foreach}
  </div>
</div>
