<?php
/*

* This file called using a cron to do optimize db

*/

include(dirname(__FILE__).'/../../config/config.inc.php');

include(dirname(__FILE__).'/../../init.php');

/* Check security token */

if (substr(Tools::encrypt('databaseoptimize/cron'), 0, 10) != Tools::getValue('token') || !Module::isInstalled('databaseoptimize'))

die('Bad token');

//v1.8 START

if (!defined('_PS_MODE_DEMO_'))

define('_PS_MODE_DEMO_', false);

//END v1.8



include(dirname(__FILE__).'/databaseoptimize.php');
// print_r($_GET);
$module = new DatabaseOptimize;
$module->optimize(Tools::getValue('t'));
